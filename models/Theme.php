<?php
class Theme {

	private $id;

	private $version;

	private $name;

	private $path;

	private $isGlobalTheme;

	private $appliesToComponents = [];

	private $cssPath = [];

	public __construct(int $id = null) {

		if($id != null) {

			$requestQuery = $GLOBALS['database']->createRequest(RequestType::REQUEST_SELECT);
			$requestQuery->setTargetTable('themes');
			$requestQuery->addCondition('id', '=', $id);
			$requestQuery->performQuery();

			$result = $requestQuery->toArray();

			if(count($result) != 1) {
				throw new DatabaseQueryError(_('Unable to find theme with specified id'));
			}

			$this->id = $id;
			$this->name = $result[0]['name'];
			$this->path = $result[0]['path'];
			$this->isGlobalTheme = ($result[0]['isGlobalTheme'] == 0) ? TRUE : FALSE;

		} else {

			$this->isGlobalTheme = ($this->isGlobalTheme == 0) ? TRUE : FALSE;

		}

	}

	public function getId() {

		return $this->id;

	}

	public function getName() {

		return $this->name;

	}

	public function getPath() {

		return $this->path;

	}

	public function getIsGlobalTheme() {

		return $this->isGlobalTheme;

	}

	public static function getAvailableThemes($onlyGlobals = false) {

		$availableThemesRequest = $GLOBALS['database']->createRequest(RequestType::REQUEST_SELECT);
		$availableThemesRequest->setTargetTable('themes');

		if($onlyGlobals) {

			$availableThemesRequest->addCondition('isGlobal', '=', 0);

		}

		$availableThemesRequest->performQuery();

		return $availableThemesRequest->toClasses(Theme:class);

	}

	public function reloadAvailableThemes($path = 'themes/') {

		$status = 0;

		$deleteThemesRequest = $GLOBALS['database']->createRequest(RequestType::REQUEST_DELETE);
		$deleteThemesRequest->setTargetTable('themes');
		$deleteThemesRequest->performRequest();

		foreach(glob($path . '*', GLOB_ONLYDIR) as $themeFolder) {

			if(file_exists($file . '/metadata.json')) {

				$themeMetadata =json_decode(file_get_contents($file . '/metadata.json'), TRUE);
				$version = $themeMetadata['version'];
				$themeName = $themeMetadata['themeName'];
				$themePath = $themeMetadata['themePath'];
				$themeIsGlobal = $themeMetadata['themeIsGlobal'];

				if(!$themeIsGlobal) {

					$themeAppliesTo = $themeMetadata['themeAppliesTo'];

				}

				if($version == NULL || $themePath == NULL || $themeIsGlobal == NULL) {

					$status = Returns::THEME_MISSING_METADATA;
					continue;

				}



			} else {

				$status = Returns::THEME_MISSING_METADATA_FILE;

			}

		}

	}

}
