<?php
/** @tableName pages **/
class Page extends DatabaseReflectedObject {

	/**
	@database key pageId
	**/
	protected $pageId;

	/**
	@database value pageName
	**/
	protected $pageName;

	/**
	@database value hasCustomUrl
	**/
	protected $hasCustomUrl;

	/**
	@database value pageEditPath
	**/
	protected $pageEditPath;

	/**
	@database value pageViewPath
	**/
	protected $pageViewPath;

	/**
	@database value pageCreationDate
	**/
	protected $pageCreationDate;

	/**
	@database value pageExpiryDate
	**/
	protected $pageExpiryDate;

	/**
	@database value isPubliclyEditable
	**/
	protected $isPubliclyEditable;

	/**
	@database joined Component pageToComponents pageId componentId
	**/
	protected $components;

	/**
	@database value componentsOrder
	**/
	protected $componentsOrder;

	/**
	@database value layoutName
	**/
	protected $layoutName;

	private $layout;

	private $componentsOrderArray;

	public function __construct($pageId) {

		$this->pageId = $pageId;
		$this->setupObject();

		$this->layout = LayoutManager::getLayout($this->layoutName, $this);


	}

	public function getPageId() {

		return $this->pageId;

	}

	/** @synced **/
	protected function setPageName($pageName) {

		if(strlen($pageName) >= 1) {

			$this->pageName = $pageName;

		}

	}

	public function getPageName() {

		return $this->pageName;

	}

	public function getPageViewPath() {

		return $this->pageViewPath;

	}

	public function getPageEditPath() {

		return $this->pageEditPath;

	}

	public function getComponents() {

		ComponentManager::getAvailableComponents();

		$componentFullClass = [];

		foreach($this->components as $component) {

			$className = $component->componentClass;
			$componentFullClass[$component->getComponentId()] = new $className($component->getComponentId());

		}

		return $componentFullClass;

	}

	public function getOrderedComponents() {

		if($this->getComponentsOrder() == null) {

			return $this->getComponents();

		}

		$components = $this->getComponents();
		$orderedComponents = [];

		foreach($this->getComponentsOrder() as $componentOrder) {

			if(!array_key_exists($componentOrder, $components)) {

				throw new Exception("Unable to find specified component :" . $componentOrder);

			}
			$orderedComponents[] = $components[$componentOrder];

		}

		return $orderedComponents;
	}

	private function getComponentsOrder() {

		if($this->componentsOrder == null) return null;

		if($this->componentsOrderArray == null) {

			$this->componentsOrderArray = explode(",", $this->componentsOrder);

		}

		return $this->componentsOrderArray;


	}

	/**  @synced **/
	protected function setComponentsOrder(array $order) {

		if(is_array($order) and count($order) == count($this->getRawComponents())) {

			foreach($this->getComponents() as $component) {

				if(!in_array($component->getComponentId(), $order)) {

					throw new Exception(_("Unable to get component ID in component order list !"));

				}

			}

			$this->componentsOrderArray = $order;
			$this->componentsOrder = implode(",", $order);

		} else {

			throw new Exception(_("Invalid component order type "));

		}

	}

	public function getRawComponents() {

		return $this->components;

	}

	/** @synced **/
	protected function removeComponent($componentId) {


		var_dump($this->getComponentsOrder());
		for($i = 0; $i < count($this->getComponentsOrder()); $i++) {

			if($this->componentsOrderArray[$i] == $componentId) {

				unset($this->componentsOrderArray[$i]);
				$this->componentsOrder = implode(",", $this->componentsOrderArray);
				$this->componentsOrderArray = null;
				break;

			}

		}


		for($i = 0; $i < count($this->getRawComponents()); $i++) {

			if($this->getRawComponents()[$i]->getComponentId() == $componentId) {


				unset($this->components[$i]);
				return;

			}

		}


		throw new Exception("Unable to find component !");

	}

	/** @synced **/
	protected function addComponent(Component $component) {

		$this->components[] = $component;

		$this->getComponentsOrder();
		$this->componentsOrderArray[] = $component->getComponentId();
		$this->componentsOrder = implode(",", $this->componentsOrderArray);


	}

	public function getLayout() {

		return $this->layout;

	}


	/** @synced **/
	protected function setPageEditPath($pageEditPath) {

		$this->pageEditPath = $pageEditPath;

	}

	/** @synced **/
	protected function setPageViewPath($pageViewPath) {

		$this->pageViewPath = $pageViewPath;

	}

	public function getPageFromEditPath($pageEditPath) {

		$pages = DatabaseReflectedObject::listObjects(get_called_class(), array(
			"pageEditPath" => $pageEditPath

		));

		if(count($pages) != 1) {

			throw new Exception('Unable to find page !');

		} else {

			return $pages[0];

		}

	}

	public function createPage() {

		$newPage = DatabaseReflectedObject::createInstance(get_called_class(),
			array(
				"pageName" => "New Page",
				"hasCustomUrl" => false,
				"pageEditPath" => "",
				"pageViewPath" => "",
				"pageCreationDate" => time(),
				"pageExpiryDate" => -1, //FIXME : use settings
				"isPubliclyEditable" => 1, //FIXME : no login yet
				"layoutName" => LayoutManager::getDefaultLayoutName()
			)
		);

		$newPage->setPageEditPath(hash('sha256', $newPage->getPageId() . 'edit' .random_bytes(32)));
		$newPage->setPageViewPath(hash('sha256', $newPage->getPageId() . 'view' .random_bytes(32)));
		$newPage->sync();

		return $newPage->getPageEditPath();
	}

}
