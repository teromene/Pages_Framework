<?php
include 'Users/autoload.php';
include 'Layout/autoload.php';
include 'Component/autoload.php';

spl_autoload_register(function ($class_name) {
	if(file_exists(dirname(__FILE__) . '/' . $class_name . '.php')) {
		include $class_name . '.php';
	}
});