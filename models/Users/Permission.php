<?php
/** @tableName permissions **/
class Permission extends DatabaseReflectedObject {

	/**
	@database key permissionId
	@var int Id of the role
	**/
	protected $permissionId;

	/**
	@database value permissionLabel
	@var string Label of the permission
	**/
	public $permissionLabel;


	public function __construct($permissionId) {

		$this->permissionId = $permissionId;
		$this->setupObject();

	}



}

class Permissions {



}
