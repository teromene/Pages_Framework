<?php
/** @tableName profile **/
class Profile extends DatabaseReflectedObject {

	/**
	@database key profileId
	@var integer the Id of the profile
	**/
	protected $profileId;

	/**
	@database value websiteURL
	@var string the URL of the user's website
	**/
	protected $websiteURL;

	/**
	@database value emailAddress
	@var string the user's email address
	**/
	protected $emailAddress;

	/**
	@database value phoneNumber
	@var string the user's phone number
	**/
	protected $phoneNumber;

	/**
	@database value showPhoneNumber
	@var boolean can use the phone number to contact
	**/
	protected $showPhoneNumber;

	/**
	@database value showEmailAddress
	@var boolean can use the email address to contact
	**/
	protected $showEmailAddress;

	/**
	@database value presentationText
	@var string the presentation text
	**/
	protected $presentationText;

	public function __construct($profileId) {

		$this->profileId = $profileId;
		$this->setupObject();

	}

	public function createUserProfile($emailAddress = '', $phoneNumber = '', $showPhoneNumber = false, $showEmailAddress = false, $presentationText = '') {

		return DatabaseReflectedObject::createInstance(get_called_class(), array(
			"emailAddress" => $emailAddress,
			"phoneNumber" => $phoneNumber,
			"showPhoneNumber" => $showPhoneNumber,
			"showEmailAddress" => $showEmailAddress,
			"presentationText" => $presentationText
		));

	}
}
