<?php
/** @tableName users **/
class User extends DatabaseReflectedObject {

	/**
	@database joined Permission userPermissions userId permissionId
	@var Permission The permissions that the user has
	**/
	protected $permissions;

	/**
	@database singleJoined Profile userProfile profileId
	@var Profile The profile associated with the user
	**/
	protected $userProfile;

	/**
	@dbt singleJoined ArtistGallery linkedGallery profileId artistGalleryId
	**/
	protected $artistGallery;

	/**
	@database key userId
	@var integer The ID that the user has, is used internally
	**/
	protected $uid;

	/**
	@database value displayName
	@var string The name displayed to the user
	**/
	protected $displayName;

	/**
	@database value username
	@var string The name of the user that is used to login
	**/
	protected $username;

	/**
	@database value password
	@var string The password of the user, hashed and salted
	**/
	protected $password;

	/**
	@database value lastLoginTime
	@var timestamp Last time the user logged in.
	**/
	protected $lastLoginTime;

	/**
	@database value isActive
	@var boolean Whether the user account is currently active or not
	**/
	protected $isActive;


	public function __construct($uid) {

		$this->uid = $uid;
		$this->setupObject();

	}

	protected function getUid() {

		return $this->uid;

	}

	protected function getIsActive() {

		return $this->isActive;

	}

	/** @synced **/
	protected function setIsActive($isActive) {

		if($GLOBALS['isLoggedIn'] and $GLOBALS['currentUser']->hasPermission(Permission::SET_USER_ACTIVE)) {

			$this->isActive = $isActive;

		} else throw new PermissionError(_("You don't have the permission to set this user active"));

	}

	/** @synced **/
	protected function setPassword($rawPassword) {

		if($GLOBALS['isLoggedIn'] and ($this->getUid() == $GLOBALS['currentUser']->getUid() or $GLOBALS['currentUser']->hasPermission(Permission::SET_GLOBAL_PASSWORD))) {
			$this->password = password_hash($rawPassword, PASSWORD_DEFAULT);
		}

	}

	/** @synced **/
	protected function createUserProfile() {

		if($this->userProfile != null && $this->userProfile instanceof Profile) {

			throw new LogicException('User already has a profile !');

		}

		$this->userProfile = Profile::createUserProfile();

	}

	public function getUserProfile() {

		return $this->userProfile;

	}


	public function verifyPassword($rawPassword) {

		return password_verify($rawPassword, $this->password);

	}


	public function getPermissions() {

		return $this->permissions;

	}

	public static function createNewUser($username, $rawPassword, $displayName) {

		if(true or $GLOBALS['isLoggedIn'] and $GLOBALS['currentUser']->hasPermission(Permission::CREATE_USER)) {

			$object = DatabaseReflectedObject::createInstance(get_called_class(), array(
				"username" => $username,
				"password" => password_hash($rawPassword, PASSWORD_DEFAULT),
				"displayName" => $displayName,
				"lastLoginTime" => time(),
				"isActive" => 1
			));

			$object->createUserProfile();

			return $object->getUid();

		} else throw new PermissionError(_('You don\'t have the permission needed to create an user '));
	}

	public static function listUsers($needIsActive = false) {

		return DatabaseReflectedObject::listObjects(get_called_class(), array(
			"isActive" => $needIsActive

		));

	}

	public static function performLoginFromPassword($username, $rawPassword) {

		//FIXME: not implemented

	}

	public static function performLoginFromCookie() {

		//FIXME: not implemented

	}

}
