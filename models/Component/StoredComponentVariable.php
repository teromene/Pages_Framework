<?php
/** @tableName storedComponentVariable **/
class StoredComponentVariable extends DatabaseReflectedObject {
	
	/** @database key storedComponentVariableId **/
	protected $storedComponentVariableId;
	
	/** @database value storedComponentVariableName **/
	protected $storedComponentVariableName;
	
	/** @database value storedComponentVariableValue **/
	protected $storedComponentVariableValue;
	
	/** @database value componentId **/
	protected $componentId;
	
	public function __construct($storedComponentVariableId) {
		
		$this->storedComponentVariableId = $storedComponentVariableId;
		$this->setupObject();
		
	}
	
	public function getVariableName() {
		
		return $this->storedComponentVariableName;
		
	}
	
	public function getVariableValue() {
		
		return $this->storedComponentVariableValue;
		
	}

	/** @synced **/
	protected function setVariableValue($storedComponentVariableValue) {
		
		$this->storedComponentVariableValue = $storedComponentVariableValue;
		
	}
	
	public function createStoredComponentVariable($storedComponentVariableName, $storedComponentVariableValue, $componentId) {
		
			return DatabaseReflectedObject::createInstance(get_called_class(),
				array(
					"storedComponentVariableName" => $storedComponentVariableName,
					"storedComponentVariableValue" => $storedComponentVariableValue,
					"componentId" => $componentId	
				)
		);
		
	}
	
}