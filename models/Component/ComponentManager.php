<?php
class ComponentManager {
	
	public static function getAvailableComponents() {
		
		$componentPath = __DIR__ . "/../../plugins/components/";
		
		$componentsInfo = [];
		
		foreach(glob($componentPath .  "*/*.php") as $filename) {
				include_once($filename);
				$filePath = explode('/', $filename);
				$className = explode(".", array_pop($filePath))[0];
				
				$classInfo = new ReflectionClass($className);
				$componentsInfo[] = array(
										"componentClass" => $className,
										"componentName" => $classInfo->getDefaultProperties()['componentName'],
										"componentDescription" => $classInfo->getDefaultProperties()['componentDescription']
									);
				
		}
		
		return $componentsInfo;
		
	}
	
	public static function createComponentFromClassName($componentClass) {
		
		self::getAvailableComponents();
		
		try {
			
			return DatabaseReflectedObject::createInstance('Component', array(
				"componentClass" => $componentClass
			));
			
			
		} catch(Exception $e) {
			
			throw new Exception("Unable to find specified component " . $e);
			
		}
		
	}
	
}