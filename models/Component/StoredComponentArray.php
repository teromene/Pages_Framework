<?php
/** @tableName storedComponentArray **/
class StoredComponentArray extends DatabaseReflectedObject {
	
	/** @database key storedComponentArrayId **/
	public $storedComponentArrayId;
	
	/** @database value componentId **/
	public $componentId;
	
	/** @database value storedComponentArrayName **/
	public $storedComponentArrayName;
	
	/** @database joined StoredComponentArrayValue storedComponentArrayToValues storedComponentArrayId storedComponentArrayValueId **/
	public $storedComponentArrayValues;
	
	public function __construct($storedComponentArrayId) {
		
		$this->storedComponentArrayId = $storedComponentArrayId;
		$this->setupObject();
		
	}
	
}

class StoredComponentArrayValue extends DatabaseReflectedObject {
	
	public $storedComponentArrayValueId;
	
	public $storedComponentArrayValueKey;
	
	public $storedComponentArrayValue;
	
	public function __construct($storedComponentArrayValueId) {
		
		$this->storedComponentArrayValueId = $storedComponentArrayValueId;
		$this->setupObject();
		
	}
	
	
	
}