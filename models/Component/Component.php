<?php
/** @tableName components **/
class Component extends DatabaseReflectedObject {
	
	/** @database joined StoredComponentVariable storedComponentVariable componentId storedComponentVariableId storedComponentVariableName **/
	protected $variableStorage = [];
	
	private $arrayVariableStorage = [];
	
	/** @database key componentId **/
	protected $componentId;
	
	/** @database value componentClass **/
	public $componentClass;
	
	protected $componentName = "Unnamed component";

	protected $componentDescription = "an unnamed component";

	public function __construct($componentId) {
		
		$this->componentId = $componentId;
		$this->setupObject();

		$this->initData();
				
	}
	
	protected function initData() {
		
		$this->variableStorage = DatabaseReflectedObject::listObjects(StoredComponentVariable::class, ["componentId" => $this->componentId]);
		$this->arrayVariableStorage = DatabaseReflectedObject::listObjects(StoredComponentArray::class, ["componentId" => $this->componentId]);

		
	}
	
	private function findData($dataName) {
		
		foreach($this->variableStorage as $variable) {
			
			if($variable->getVariableName() == $dataName) return $variable;
			
		}
		
		return NULL;
		
	}
	
	/** @synced **/
	protected function storeModuleData($dataName, $dataValue) {
		
		$variable = $this->findData($dataName);
		
		if($variable != NULL) {
			
			$variable->setVariableValue($dataValue);
			
		} else {
			
			$this->variableStorage[$dataName] = StoredComponentVariable::createStoredComponentVariable($dataName, $dataValue, $this->getComponentId());
			
		}
		
		
	}
	
	protected function getModuleData($dataName, $defaultValue = null) {

		$variable = $this->findData($dataName);
	
		return ($variable == NULL) ? $defaultValue : $variable->getVariableValue();	
		
	}
	
	public function getComponentId() {
		
		return $this->componentId;
		
	}
	
	public function getComponentName() {
		
		return $this->componentName;
		
	}
	
	public function getComponentDescription() {
		
		return $this->componentDescription;
		
	}
	
	public function getEditorView() {
		
		return "";
		
	}
	
	public function getEditorForm() {
		
		return "";
		
	}
	
	public function getPageView() {
		
		return "";
		
	}
	
	public function processEditorFormData($get, $post, $file) {
		
		
	}
	
	public function processPageViewFormData() {
		
		return "";
	}
}