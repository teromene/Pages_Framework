<?php
class LinearLayout implements Layout {
	
	private $page;
	
	public function __construct($page) {
		
		$this->page = $page;
		
	}
	
	public function getLayoutName() {
		
		return "LinearLayout";
		
	}
	
	public function getEditorIncludes() {
		
		return "<link href='" . Utils::getBaseUrl() . "/static/style/linearLayout.css' rel='stylesheet' type='text/css'>";
		
	}
	
	public function getViewIncludes() {
		
		return "";
		
	}
	
	public function getEditorLayoutStart() {
		
		return "<section class='linearlayout-editor-section box-big'>";
		
	}
	
	public function getViewEditorLayoutStart() {
		
		return "";
		
	}
	
	public function getEditorLayoutEnd() {
		
		return "</section>";
		
	}
	
	public function getViewLayoutEnd() {
		
		return "";
		
	}
	
	public function getEditorComponent(Component $component) {
		
		return "<section class='linearlayout-editor-component'><div class='linearlayout-editor-component-toolbar'>"
			."<p class='linearlayout-editor-component-title'>"
			. $component->getComponentName()
			. "</p><p class='linearlayout-editor-component-options'>"
			. "<a href='#' data-component='"
			. $component->getComponentId()
			. "' class='editor-component-edit'> Edit component"
			. " </a>| <a href='"
			. Utils::getBaseUrl() . '/removeComponent/' . $this->page->getPageEditPath() . '/' . $component->getComponentId()
			."'>Remove component</a></p></div>"
			. $component->getEditorView()
			. "</section>";
		
	}
	
	public function getViewComponent(Component $component) {
		
		return "";
		
	}

	
	
}
