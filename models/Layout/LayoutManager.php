<?php
class LayoutManager {
	
	private function __construct() {}
	
	public static function getAvailableLayouts() {
	
		$layoutClasses = [];
	
		foreach(get_declared_classes() as $potentialClass) {
			
			if(in_array('Layout', class_implements($potentialClass, true))) {
				
				$layoutClasses[] = $potentialClass;
				
			}
			
		}
		
	}
	
	public static function getLayout($layoutName, $page) {
		
		try {
			
			$layout = new $layoutName($page);

			return $layout;
			
		} catch(Exception $e) {
			
			throw new Exception("Unable to load layout " . $layoutName);
			
		}
		
	}
	
	public static function getDefaultLayoutName() {
		
			return "LinearLayout";
			
	}
	
}