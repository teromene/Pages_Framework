<?php
interface Layout {

	public function getLayoutName();
	
	public function getEditorIncludes();
	
	public function getViewIncludes();
	
	public function getEditorLayoutStart();
	
	public function getViewEditorLayoutStart();
	
	public function getEditorLayoutEnd();
	
	public function getViewLayoutEnd();
	
	public function getEditorComponent(Component $component);
	
	public function getViewComponent(Component $component);
	
}