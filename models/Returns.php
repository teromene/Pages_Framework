<?php
class Returns {

	const SUCCESS = 0;
	const FAILURE = -1;

	//Function specific return codes

	//Themes
	const THEME_MISSING_METADATA_FILE = 11;
	const THEME_MISSING_METADATA = 12;

}
