window.onload = function() {

	var editorElements = document.getElementsByClassName("editor-component-edit");
	for(var i = 0; i < editorElements.length; i++) {

		editorElements[i].onclick = function(event) {
			showEditorModal(event)
		}

	}


	document.getElementById("editor-componentsettings-close").onclick = hideEditorModal;

	document.getElementById("editor-addcomponent-close").onclick = hideComponentAddModal;

	document.getElementById("editor-rename-page-button").onclick = showRenamePageModal;

	document.getElementById("editor-renamepage-close").onclick = hideRenamePageModal;

	document.getElementById("editor-component-add").onclick = showAddComponentModal;

	document.getElementById("editor-componentsettings-iframe").onload = function(event) {
		autoHideEditorModal(event);
	}

	editorZone = document.getElementById("editor-zone").children[0];

	Sortable.create(editorZone,
		{
			onEnd : reorderItems
		}
	);

}

function reorderItems() {

	orderList = [];

	for(i = 0; i<editorZone.children.length; i++) {
		orderList[i] = editorZone.children[i].getElementsByClassName("editor-component-edit")[0].getAttribute("data-component");
	}

	orderString = JSON.stringify(orderList);

	var form = document.createElement("form");
	form.setAttribute("method", "post");
	form.setAttribute("action", "#");

	var orderField = document.createElement("input");
	orderField.setAttribute("type", "hidden");
	orderField.setAttribute("name", "element-order");
	orderField.setAttribute("value", orderString);
	form.appendChild(orderField);

	document.body.appendChild(form);
	form.submit();

}

function showEditorModal(event) {

	var componentId = event.target.getAttribute('data-component');
	document.getElementById("editor-componentsettings-container").style.display = "block";
	document.getElementById("editor-componentsettings-iframe").src = pagePath + "/" + componentId;

}

function hideEditorModal() {

	document.getElementById("editor-componentsettings-container").style.display = "none";
	document.getElementById("editor-componentsettings-iframe").src = "";

}

function autoHideEditorModal(event) {

	if(event.target.contentDocument.body.innerHTML == "<span class=\"endload\"></span>") location.reload(true);

}

function showAddComponentModal() {

	document.getElementById("editor-addcomponent-container").style.display = "block";

}

function hideComponentAddModal() {

	document.getElementById("editor-addcomponent-container").style.display = "none";

}

function hideRenamePageModal() {

	document.getElementById("editor-renamepage-container").style.display = "none";

}

function showRenamePageModal() {

	document.getElementById("editor-renamepage-container").style.display = "block";

}
