<?php
require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/lib/autoload.php';
require __DIR__ . '/models/autoload.php';

$cm = new ConfigurationManager();

$GLOBALS['database'] = $cm->getDatabase();

$app = $cm->generateApp();
$GLOBALS['app'] = $app;

require __DIR__ . '/routes/autoload.php';

$app->run();
