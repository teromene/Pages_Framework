<?php

class TextComponent extends Component {
	
	protected $componentName = "Text component";
	
	protected $componentDescription = "A simple text component";
	
	public $viewTemplatePath = "";
	
	public $editorTemplatePath = "";
	

	public function getEditorView() {
		
		$title = $this->getModuleData('title', "Insert your title here");
		$text  = $this->getModuleData('text', "Insert your text here");
		
		return <<<EOD
<h3 class='TextComponent-title'>
$title
</h3>
<div class='TextComponent-text'>
$text
</div>
EOD;
		
	}
	
	public function getEditorForm() {
		
		$title = $this->getModuleData('title', "Insert your title here");
		$text  = $this->getModuleData('text', "Insert your text here");

		
		return <<<EOD
<label for="text-title">Title</label>
<br />
<input type="text" name="text-title" value="$title"/>
<br />
<label for="text-content">Content</label>
<br /><textarea name="text-content" >$text</textarea>
EOD;
		
	}
	
	public function processEditorFormData($get, $post, $file) {
		
		if(isset($post['text-title']) and $post['text-title'] != '') {
			
			$this->storeModuleData('title', $post['text-title']);
			
		}
		
		if(isset($post['text-content']) and $post['text-content'] != '') {
			
			$this->storeModuleData('text', $post['text-content']);
			
		}	
		
	}


	
}