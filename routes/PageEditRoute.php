<?php

$app->get('/create/', function ($request, $response, $args) {

	header('Location: ' . dirname($_SERVER['PHP_SELF']) . '/edit/'. Page::createPage());
	exit;

});

$app->get('/edit/{pageEditPath}',  function ($request, $response, $args) {

	$currentPage = Page::getPageFromEditPath($args['pageEditPath']);
	return PageEditor::renderPageEditor($currentPage);

});

$app->post('/edit/{pageEditPath}',  function ($request, $response, $args) {

	$currentPage = Page::getPageFromEditPath($args['pageEditPath']);
	if(array_key_exists("element-order", $_POST)) {

		try {

			$currentPage->setComponentsOrder(json_decode($_POST['element-order']));

		} catch(Exception $e) {

			die("Unable to parse order !");

		}
	}

	header('Location: ' . Utils::getBaseUrl() . '/edit/' . $currentPage->getPageEditPath());
	die;

});

$app->post('/renamePage/{pageEditPath}',  function ($request, $response, $args) {

	$currentPage = Page::getPageFromEditPath($args['pageEditPath']);

	if(array_key_exists("page-name-edit", $_POST) and strlen($_POST["page-name-edit"]) >= 1) {

		$currentPage->setPageName($_POST["page-name-edit"]);

	}

	header('Location: ' . Utils::getBaseUrl() . '/edit/' . $currentPage->getPageEditPath());
	exit;

});


$app->get('/editComponent/{pageEditPath}/{componentId}', function ($request, $response, $args) {

	$currentPage = Page::getPageFromEditPath($args['pageEditPath']);
	return PageEditor::renderComponentSettings($currentPage, $args['componentId']);

});

$app->post('/editComponent/{pageEditPath}/{componentId}', function ($request, $response, $args) {

	$currentPage = Page::getPageFromEditPath($args['pageEditPath']);
	PageEditor::callUploadFunction($currentPage, $args['componentId'], $_GET, $_POST, $_FILE);
	return "<span class='endload'></span>";

});

$app->get('/addComponent/{pageEditPath}/{componentClass}', function ($request, $response, $args) {

	$currentPage = Page::getPageFromEditPath($args['pageEditPath']);
	$currentPage->addComponent(ComponentManager::createComponentFromClassName($args['componentClass']));
	header('Location: ' . Utils::getBaseUrl() . '/edit/' . $currentPage->getPageEditPath());
	exit;

});

$app->get('/removeComponent/{pageEditPath}/{componentId}', function ($request, $response, $args) {

	$currentPage = Page::getPageFromEditPath($args['pageEditPath']);
	$currentPage->removeComponent($args['componentId']);
	header('Location: ' . Utils::getBaseUrl() . '/edit/' . $currentPage->getPageEditPath());
	exit;

});
