<?php
$app->get('/view/[{pageId}]', function ($request, $response, $args) {

	var_dump($args['pageId']);

    return "";
	
});

$app->get('/version/', function ($request, $response, $args) {

	return 'v0.5';

});


$app->get('/test/', function ($request, $response, $args) {

	foreach(User::listUsers(true) as $user) {

		if($user->getUserProfile() == null) {

			$user->createUserProfile();
			var_dump("Setting up profile....");

		} else {

			var_dump("Profile already OK");

		}

	}

});

$app->get('/views/', function ($request, $response, $args) {

	return $this->view->render($response, 'index.phtml');

});
