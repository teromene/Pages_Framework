<?php declare(strict_types = 1);
if(getenv('GITLAB_RUNNER') == "true") echo "WARNING: GITLAB_RUNNER is set to true: assertException, assertThrowable and assertError will be skipped.\n";

trait AssertException
{

	/**
	 * @param callable $test
	 * @param string|null $expectedThrowableClass
	 * @param string|int|null $expectedCode
	 * @param string|null $expectedMessage
	 * @return \Throwable
	 */
	public static function assertThrowable(
		callable $test,
		string $expectedThrowableClass = Throwable::class,
		$expectedCode = null,
		string $expectedMessage = null
	)
	{
		if(getenv('GITLAB_RUNNER') == "false") {

			VladaHejda\AssertException::assertError($test, $expectedThrowableClass, $expectedCode, $expectedMessage);

		}
	}

	/**
	 * @param callable $test
	 * @param string|null $expectedExceptionClass
	 * @param string|int|null $expectedCode
	 * @param string|null $expectedMessage
	 * @return \Exception
	 */

	public static function assertException(
		callable $test,
		string $expectedExceptionClass = Exception::class,
		$expectedCode = null,
		string $expectedMessage = null
	)
	{
		if(getenv('GITLAB_RUNNER') == "false") {

			VladaHejda\AssertException::assertException($test, $expectedExceptionClass, $expectedCode, $expectedMessage);

		}
	}

	/**
	 * @param callable $test
	 * @param string|null $expectedErrorClass
	 * @param string|int|null $expectedCode
	 * @param string|null $expectedMessage
	 * @return \Error
	 */
	public static function assertError(
		callable $test,
		string $expectedErrorClass = Error::class,
		$expectedCode = null,
		string $expectedMessage = null
	)
	{
		if(getenv('GITLAB_RUNNER') == "false") {

			VladaHejda\AssertException::assertError($test, $expectedErrorClass, $expectedCode, $expectedMessage);

		}
	}
}

