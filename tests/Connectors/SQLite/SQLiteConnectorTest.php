<?php
use phpunit\framework\TestCase;

class SQLiteConnectorTest extends TestCase {


	use AssertException;

	/**
	* Tests wether the function throws an exception when loading a non-existing file.
	* @expectedException PDOException
	*/
	public function testConnectionToNonExistingFile() {

		$c = new SQLiteConnector('WrongSQLiteConnectorDatabase.sqlite');

	}

	/**
	* Tests that the connector works on an existing file.
	*/
	public function testConnection() {

		$c = new SQLiteConnector(__DIR__ . '/SQLiteConnectorDatabase.sqlite');

	}

	/**
	* Tests the getRows function on an existing row.
	*/
	public function testGetRowsExisting() {

		$connection = new SQLiteConnector(__DIR__ . '/SQLiteConnectorDatabase.sqlite');

		$request = $connection->createRequest(RequestType::REQUEST_SELECT);
		$request->setTargetTable('testGetRows');
		$request->performRequest();
		$result = $request->toArray();

		$this->assertNotNull($result, 'The query should return results');
		$this->assertCount(45, $result, 'The array size is not the expected one');

		$request = $connection->createRequest(RequestType::REQUEST_SELECT);
		$request->setTargetTable('testGetRows');
		$request->setLimit(10);
		$request->performRequest();
		$result = $request->toArray();


		$this->assertNotNull($result, 'The query should return results');
		$this->assertCount(10, $result, 'The array size is not the expected one');

	}

	/**
	* Tests the getRows function on an non-existing row.
	* @expectedException DatabaseQueryError
	*/
	public function testGetRowsNotExisting() {

		$connection = new SQLiteConnector(__DIR__ . '/SQLiteConnectorDatabase.sqlite');

		$connection->createRequest(RequestType::REQUEST_SELECT)->setTargetTable('nonExistingRow');

	}

	/**
	* Tests the getRowsWithId function.
	*/
	public function testGetRowsWithId() {

		$connection = new SQLiteConnector(__DIR__ . '/SQLiteConnectorDatabase.sqlite');
		$request = $connection->createRequest(RequestType::REQUEST_SELECT);
		$request->setTargetTable('testgetrowsid');
		$request->addCondition('id', '=', '1');
		$result = $request->toArray();

		$this->assertCount(1, $result, 'Missing or too much row in the resulting array');
		$this->assertArrayHasKey('name', $result[0], 'Missing information in the resulting array');

		$request = $connection->createRequest(RequestType::REQUEST_SELECT);
		$request->setTargetTable('testgetrowsid');
		$request->addCondition('id', '=', '7');
		$result = $request->toArray();

		$this->assertCount(0, $result, 'Shouldn\'t have any results when ID does not exists');
		
		$this->assertException(function() {
			$request->addCondition('not-existing-id', '=', '7');
		});


	}


	/**
	* Test the insertInto function.
	*/
	public function testInsertInto() {

		copy(__DIR__ . '/SQLiteConnectorDatabase.sqlite', __DIR__ . '/SQLiteConnectorDatabase_modify.sqlite');
		$connection = new SQLiteConnector(__DIR__ . '/SQLiteConnectorDatabase_modify.sqlite');

		$insertQuery = $connection->createRequest(RequestType::REQUEST_INSERT);
		$insertQuery->setTargetTable('testGetRows');
		$insertQuery->setInsertedFields(['row1', 'row2', 'row3']);
		$insertQuery->setValues(['lala', 'lili', 'lolo']);
		$insertQuery->insert();

		//Check if the row was inserted
		$requestQuery = $connection->createRequest(RequestType::REQUEST_SELECT);
		$requestQuery->setTargetTable('testGetRows');
		$requestQuery->addCondition('row1', '=', 'lala');
		$requestQuery->performRequest();

		$this->assertEquals(['row1' => 'lala', 'row2' => 'lili', 'row3' => 'lolo'], $requestQuery->toArray()[0], 'The row was not inserted');

	}

	/**
	* Test the insertIntoAndReturnId function.
	*/
	public function testInsertIntoAndReturnId() {

		copy(__DIR__ . '/SQLiteConnectorDatabase.sqlite', __DIR__ . '/SQLiteConnectorDatabase_modify.sqlite');
		$connection = new SQLiteConnector(__DIR__ . '/SQLiteConnectorDatabase_modify.sqlite');

		$insertQuery = $connection->createRequest(RequestType::REQUEST_INSERT);
		$insertQuery->setTargetTable('testgetrowsid');
		$insertQuery->setInsertedFields(['name', 'test']);
		$insertQuery->setValues(['aaa', 'bbb']);
		$insertQuery->insert();

		$this->assertEquals(6, $insertQuery->getLastInsertId(), 'The auto-increment value was not found');

	}

	/**
	* Test the delete transaction request
	*/
	public function testDeleteTransaction() {

		copy(__DIR__ . '/SQLiteConnectorDatabase.sqlite', __DIR__ . '/SQLiteConnectorDatabase_modify.sqlite');
		$connection = new SQLiteConnector(__DIR__ . '/SQLiteConnectorDatabase_modify.sqlite');

		$deleteQuery = $connection->createRequest(RequestType::REQUEST_DELETE);
		$deleteQuery->setTargetTable('testGetRows');
		$deleteQuery->performRequest();

		$elementList = $connection->createRequest(RequestType::REQUEST_SELECT);
		$elementList->setTargetTable('testGetRows');
		$elementList->performRequest();

		$this->assertEquals([], $elementList->toArray(), 'After deleting, the array should be null');

		copy(__DIR__ . '/SQLiteConnectorDatabase.sqlite', __DIR__ . '/SQLiteConnectorDatabase_modify.sqlite');
		$connection = new SQLiteConnector(__DIR__ . '/SQLiteConnectorDatabase_modify.sqlite');

		$deleteQuery = $connection->createRequest(RequestType::REQUEST_DELETE);
		$deleteQuery->setTargetTable('testgetrowsid');
		$deleteQuery->addCondition('id', '>', 2);
		$deleteQuery->performRequest();

		$elementList = $connection->createRequest(RequestType::REQUEST_SELECT);
		$elementList->setTargetTable('testgetrowsid');
		$elementList->performRequest();

		$this->assertCount(2, $elementList->toArray(), 'We deleted 3 elements out of 5.');

	}

}
