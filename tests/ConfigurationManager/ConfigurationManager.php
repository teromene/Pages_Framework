<?php
/**
* Testing the way the configuration manager behaves
*/

/**
* Unit tests for ConfigurationManager
*/

use PHPUnit\Framework\TestCase;

class ConfigurationManagerTest extends PHPUnit_Framework_TestCase {

	/**
	* Tests wether the app throws an exception when trying to load a file with name null
	* @expectedException ConfigurationErrorException
	*/
	public function testNullFileNameConfiguration() {

		$configuration = new ConfigurationManager('');

	}

	/**
	* Tests wether the app throws an exception when trying to load a file that doesn't exists.
	* @expectedException ConfigurationErrorException
	*/
	public function testFileNotExistsConfiguration() {

		$configuration = new ConfigurationManager('notexistingfile');

	}

	/**
	* Tests wether the app throws an exception when trying to load an empty file.
	* @expectedException ConfigurationErrorException
	*/
	public function testEmptyFileConfiguration() {

		$configuration = new ConfigurationManager(__DIR__ . '/configurations/emptyFile.json');

	}

	/**
	* Tests wether the app throws an exception when trying to load an empty valid json file.
	* @expectedException ConfigurationErrorException
	*/
	public function testEmptyJsonFileConfiguration() {

		$configuration = new ConfigurationManager(__DIR__ . '/configurations/emptyJson.json');

	}

	/**
	* Tests that the default values are loaded correctly when loading a json file with only DB set (Sqlite).
	*/
	public function testDefaultValuesSqliteConfiguration() {

		/*We don't want to connect straight away, as the sqlite3 file does not exists. */
		$configuration = new ConfigurationManager(__DIR__ . '/configurations/simpleConfSqlite.json', FALSE);

		$this->assertEquals($configuration->getInstanceName(), 'My Pages instance', 'The instance name is not matching default');
		$this->assertEquals('database.sqlite3', $configuration->getDatabaseFilename(), 'The filename of the database is not valid !');
		$this->assertNull($configuration->getDatabase(), 'The database socket should be null, as we haven\'t asked to connect yet');
		$this->assertNull($configuration->getDatabaseUsername(), 'The MySQL field username should be null');
		$this->assertNull($configuration->getDatabasePassword(), 'The MySQL field password should be null');
		$this->assertNull($configuration->getDatabaseHost(), 'The MySQL field host should be null');
		$this->assertNull($configuration->getDatabaseName(), 'The MySQL field database should be null');

		$this->assertFalse($configuration->getAllowCustomUrl(), 'Custom URLS shouldn\'t be allowed');
		$this->assertEquals($configuration->getCustomUrlChars(), ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 0, 1, 2, 3, 4, 5, 6, 7, 8, 9], 'The default chars is not valid');
		$this->assertEquals($configuration->getCustomCharsCount(), 48, 'The number of chars is different from what was expected');


	}

	/**
	* Tests that the default values are loaded correctly when loading a json file with only DB set (MySQL).
	*/
	public function testDefaultValuesMySQLConfiguration() {

		/*We don't want to connect straight away, as they might not be a mysql server listening. */
		$configuration = new ConfigurationManager(__DIR__ . '/configurations/simpleConfMySQL.json', FALSE);

		$this->assertEquals($configuration->getInstanceName(), 'My Pages instance', 'The instance name is not matching default');
		$this->assertNull($configuration->getDatabaseFilename(), 'The SQLite field filename should be null');
		$this->assertNull($configuration->getDatabase(), 'The database socket should be null, as we haven\'t asked to connect yet');

		$this->assertEquals($configuration->getDatabaseUsername(), 'test', 'The MySQL field username is not valid');
		$this->assertEquals($configuration->getDatabasePassword(), 'password', 'The MySQL field username is not valid');
		$this->assertEquals($configuration->getDatabaseHost(), 'localhost', 'The MySQL field username is not valid');
		$this->assertEquals($configuration->getDatabaseName(), 'db', 'The MySQL field username is not valid');

		$this->assertFalse($configuration->getAllowCustomUrl(), 'Custom URLS shouldn\'t be allowed');
		$this->assertEquals($configuration->getCustomUrlChars(), ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 0, 1, 2, 3, 4, 5, 6, 7, 8, 9], 'The default chars is not valid');
		$this->assertEquals($configuration->getCustomCharsCount(), 48, 'The number of chars is different from what was expected');


	}

}

