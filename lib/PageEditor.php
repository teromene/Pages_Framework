<?php
class PageEditor {

	public static function renderPageEditor($page) {

		return $GLOBALS['app']->getContainer()->view->fetch('pageEditor.phtml',
			array(
				'page' => $page
			)
		);

	}

	
	public static function renderComponentSettings($page, $componentId) {
		
		 return $GLOBALS['app']->getContainer()->view->fetch('componentSettings.phtml',
				array(
					'component' => self::getPageComponent($page, $componentId),
					'page' => $page
				)
		);
				
	}

	
	public static function callUploadFunction($page, $componentId, $get, $post, $file) {
		
		//FIXME: auto-process file uploading, put in $file array of uploaded path string
		self::getPageComponent($page, $componentId)->processEditorFormData($get, $post, NULL);
		
	}
	
	private static function getPageComponent($page, $componentId) {
		
		foreach($page->getComponents() as $component) {
			
			if($component->getComponentId() == $componentId) {

					return $component;
				
			}
			
		}
		
		throw new Exception("Component not found !");

	}
	
}
