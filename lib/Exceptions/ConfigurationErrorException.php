<?php
class ConfigurationErrorException extends \Exception {

	//FIXME: call parent constructor ?
	public function __construct($message, $code = 0, Exception $previous = null) {

		parent::__construct($message, $code, $previous);

	}

	public function __toString() {

		return 'Configuration error : ' . $this->message;

	}

}
