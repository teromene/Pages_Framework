<?php
class TwigConstantCall extends Twig_Extension
{

    public function getFunctions()
    {
        return array(
            new Twig_SimpleFunction('staticMethod', [$this, 'staticMethod']),
        );
		
    }
	
    public function staticMethod($className, $methodName, array $args = [])
    {

		$reflectionClass = new ReflectionClass($className);
		
        if ($reflectionClass->hasMethod($methodName) && $reflectionClass->getMethod($methodName)->isStatic() && $reflectionClass->getMethod($methodName)->isPublic()) {
			return call_user_func_array($className.'::'.$methodName, $args);
        }
		
        throw new \RuntimeException("Invalid method called");
    }
	
}