<?php
class SQLiteConnector implements DatabaseConnector {

	public $connector;

	protected $tables;

	protected $tableRows;

	public function  __construct($filename) {

		if(!file_exists($filename)) {

			throw new PDOException(_('Unable to find the database file'));

		}

		$this->connector = new PDO('sqlite:' . $filename);
		$this->connector->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
		$this->connector = new PDO('sqlite:' . $filename);

		$tablesQuery = $this->connector->query('SELECT name FROM sqlite_master WHERE type=\'table\'');

		$this->tables = $tablesQuery->fetchAll(PDO::FETCH_COLUMN);
		$this->tableRows = [];

	}

	public function createRequest($requestType) {

		if($requestType == RequestType::REQUEST_SELECT) {

			return new SQLiteSelectRequest($this);

		} else if($requestType == RequestType::REQUEST_INSERT) {

			return new SQLiteInsertRequest($this);

		} else if($requestType == RequestType::REQUEST_DELETE) {

			return new SQLiteDeleteRequest($this);

		} else if($requestType == RequestType::REQUEST_UPDATE) {

			return new SQLiteUpdateRequest($this);

		} else {

			throw new DatabaseQueryError(_('Invalid request type'));

		}

	}

	public function isValidTableName(string $tableName) {

		return in_array($tableName, $this->tables);

	}

	public function isValidFieldName(string $tableName, string $fieldName) {

 		if(!$this->isValidTableName($tableName)) {

 			throw new DatabaseQueryError(_('Wrong table name in your query'));

 		}

 		if(!array_key_exists($tableName, $this->tableRows)) { 

			$fieldsQuery = $this->connector->query('PRAGMA table_info(' . $tableName . ');');
 			$this->tableRows[$tableName] = $fieldsQuery->fetchAll(PDO::FETCH_COLUMN, 1);

 		}

 		return in_array($fieldName, $this->tableRows[$tableName]);
 	}

}
