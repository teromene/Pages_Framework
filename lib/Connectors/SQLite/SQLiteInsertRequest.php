<?php
class SQLiteInsertRequest {

	private $tableName;

	private $connector;

	private $readOnly = FALSE;

	private $requestedFields = NULL;

	private $values = array();

	private $ignoreExisting = false;


	public function __construct(SQLiteConnector $connector) {

		$this->connector = $connector;

	}

	public function setTargetTable(string $tableName) {

		if(!$this->connector->isValidTableName($tableName)) {

			throw new DatabaseQueryError(_('Invalid table name for insert query'));

		}

		$this->tableName = $tableName;

	}

	public function setInsertedFields(array $requestedFields) {

		if($this->tableName == NULL || $this->readOnly) {

			throw new DatabaseQueryError(_('Unable to modify requested fields'));

		}

		foreach($requestedFields as $requestedField) {

			if(!$this->connector->isValidFieldName($this->tableName, $requestedField)) {

				throw new DatabaseQueryError(_('Invalid field name in your insert query'));

			}

		}

		$this->requestedFields = $requestedFields;

	}

	public function setValues(array $values) {

		if($this->tableName == NULL || $this->readOnly) {

			throw new DatabaseQueryError(_('Unable to modify requested fields'));

		}

		if($this->requestedFields != NULL && count($this->requestedFields) != count($values)) {

			throw new DatabaseQueryError(_('Count mismatch between values and selected fields'));

		}

		$this->values = $values;

	}


	public function setIgnoreExisting(bool $ignoreExisting) {

		$this->ignoreExisting = $ignoreExisting;

	}

	public function insert() {

		$this->readOnly = TRUE;


		$request = 'INSERT ';
		if($this->ignoreExisting) $request .= ' IGNORE ';

		$request .= ' INTO ' . $this->tableName;

		if($this->requestedFields != NULL) {

			$request .= '(' . implode(',', $this->requestedFields) . ')';

		}

		$request .= ' VALUES (';

		for($i = 0; $i < count($this->values); $i++) {

			$request .= '? ,';

		}

		$request = rtrim($request, ',') .');';

		foreach($this->values as $key => $value) {

			if(is_bool($value)) {
				$this->values[$key] = ($value) ? 1 : 0;
			}

		}

		$insertQuery = $this->connector->connector->prepare($request);
		$insertQuery->execute($this->values);

	}

	public function getLastInsertId() {

		if(!$this->readOnly) {

			throw new DatabaseQueryError(_('Unable to get insert id of a not running query'));

		}

		return $this->connector->connector->lastInsertId();

	}
}
