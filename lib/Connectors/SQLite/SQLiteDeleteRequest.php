<?php
class SQLiteDeleteRequest {

	private static $OPERATORS = ['=', '!=', '>=', '<=', '>', '<'];

	private static $LOGIC_OPERATORS = ['AND', 'OR'];

	private $readOnly = FALSE;

	private $connector = NULL;

	private $tableName = NULL;

	private $conditionString = '';

	private $conditionElements = [];


	public function __construct($connector) {

		$this->connector = $connector;

	}

	private function checkPerformRequest() {

		if($this->readOnly) {

			throw new DatabaseQueryError(_('Unable to modify a read-only request'));

		}

		if($this->tableName == NULL) {

			throw new DatabaseQueryError(_('Unable to set requested fields on an empty table name'));

		}

	}

	public function setTargetTable(string $tableName) {

		if($this->connector->isValidTableName($tableName)) {

			$this->tableName = $tableName;

		} else {

			throw new DatabaseQueryError(_('Invalid table name'));

		}

	}


	public function addCondition(string $fieldName, string $operator, $value, $logicOperator = null) {

		$this->checkPerformRequest();

		if($logicOperator == null && $this->conditionString != '') {

			throw new DatabaseQueryError(('Unable to add a new condition without a logic operator'));

		}

		if($logicOperator != null && $this->conditionString == '') {

			throw new DatabaseQueryError(_('Unable to create a condition starting with a logic operator'));

		}

		if(!$this->connector->isValidFieldName($this->tableName, $fieldName)) {

			throw new DatabaseQueryError(_('Invalid field name used in condition.'));

		}

		if($logicOperator != NULL && !in_array($logicOperator, self::$LOGIC_OPERATORS)) {

			throw new DatabaseQueryError(_('Invalid logic operator'));

		}

		if(!in_array($operator, self::$OPERATORS)) {

			throw new DatabaseQueryError(_('Invalid condition operator'));

		}

		$temporaryConditionString = '';

		if($this->conditionString == '') {

			$temporaryConditionString .= ' WHERE ';

		} else if($logicOperator != NULL) {

			$temporaryConditionString .= ' ' . $logicOperator . ' ';

		}

		$temporaryConditionString .= $fieldName . ' ' . $operator . ' ';
		$temporaryConditionString .= ':cond' . count($this->conditionElements) .' ';

		$this->conditionString .= $temporaryConditionString;

		$this->conditionElements['cond' . count($this->conditionElements)] = $value;


	}


	public function getRequestString() {

		$requestString = 'DELETE ';
		$requestString .= ' FROM ' . $this->tableName . ' ';

		if($this->conditionString != '') {

			$requestString .= $this->conditionString;

		}

		return $requestString;

	}

	public function performRequest() {

		$request = $this->connector->connector->prepare($this->getRequestString());
		$request->execute($this->conditionElements);

		$this->readOnly = TRUE;

	}



}
