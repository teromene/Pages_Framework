<?php
class SQLiteSelectRequest {

	private static $OPERATORS = ['=', '!=', '>=', '<=', '>', '<'];

	private static $LOGIC_OPERATORS = ['AND', 'OR'];

	private $readOnly = FALSE;

	private $connector = NULL;

	private $resultElement = NULL;

	private $tableName = NULL;

	private $fieldsName = NULL;

	private $conditionString = '';

	private $conditionElements = [];

	private $limit = NULL;

	private $order = NULL;

	private $joinTable = NULL;

	private $joinConditionString = '';

	public function __construct($connector) {

		$this->connector = $connector;

	}

	private function checkPerformRequest() {

		if($this->readOnly) {

			throw new DatabaseQueryError(_('Unable to modify a read-only request'));

		}

		if($this->tableName == NULL) {

			throw new DatabaseQueryError(_('Unable to set requested fields on an empty table name'));

		}

	}

	public function setTargetTable(string $tableName) {

		if($this->connector->isValidTableName($tableName)) {

			$this->tableName = $tableName;

		} else {

			throw new DatabaseQueryError(_('Invalid table name'));

		}

	}

	public function setJoinTable(string $tableName) {

		if($this->connector->isValidTableName($tableName)) {

			$this->joinTable = $tableName;

		} else {

			throw new DatabaseQueryError(_('Invalid table name'));

		}

	}

	public function setJoinCondition(string $firstFieldName, string $operator, string $secondFieldName) {

		if(!$this->connector->isValidFieldName($this->tableName, $firstFieldName)) {

			throw new DatabaseQueryError(_('Invalid first field name in join condition'));

		}

		if(!$this->connector->isValidFieldName($this->joinTable, $secondFieldName)) {

			throw new DatabaseQueryError(_('Invalid second field name in join condition'));

		}

		if(!in_array($operator, self::OPERATORS)) {

			throw new DatabaseQueryError(_('Invalid operator in join condition'));

		}

		$this->joinConditionString = $this->table . $firstFieldName . $operator . $this->joinTable . $secondFieldName;

	}

	public function setRequestedFields(array $requestedFields) {

		$this->checkPerformRequest();

		foreach($requestedFields as $requestedField) {

			if(!$this->connector->isValidFieldName($this->tableName, $requestedField)) {

				throw new DatabaseQueryError(_('Invalid field name used.'));

			}

		}

		$this->fieldsName = $requestedFields;

	}

	public function setLimit(int $limit) {

		$this->checkPerformRequest();

		if(!is_int($limit)) {

			throw new DatabaseQueryError(_('Unable to set a non-integer limit'));

		}

		$this->limit = $limit;

	}

	public function setOrder(RequestType $requestOrder, string $fieldName) {

		$this->checkPerformRequest();

		if($requestOrder != RequestType::ORDER_RANDOM && $fieldName == NULL) {

			throw new DatabaseQueryError(_('Unable to select order without a field to order on'));

		}

		if($this->connector->isValidFieldName($requestedField, $this->fieldName)) {

			throw new DatabaseQueryError(_('Invalid sorting field name used.'));

		}

		$this->order = array('order' => $requestOrder, 'fieldName' => $fieldName);

	}

	public function addCondition(string $fieldName, string $operator, $value, $logicOperator = null) {

		$this->checkPerformRequest();

		if($logicOperator == null && $this->conditionString != '') {

			throw new DatabaseQueryError(('Unable to add a new condition without a logic operator'));

		}

		if($logicOperator != null && $this->conditionString == '') {

			throw new DatabaseQueryError(_('Unable to create a condition starting with a logic operator'));

		}

		if(!$this->connector->isValidFieldName($this->tableName, $fieldName)) {

			throw new DatabaseQueryError(_('Invalid field name used in condition.'));

		}

		if($logicOperator != NULL && !in_array($logicOperator, self::LOGIC_OPERATORS)) {

			throw new DatabaseQueryError(_('Invalid logic operator'));

		}

		if(!in_array($operator, self::$OPERATORS)) {

			throw new DatabaseQueryError(_('Invalid condition operator'));

		}

		$temporaryConditionString = '';

		if($this->conditionString == '') {

			$temporaryConditionString .= ' WHERE ';

		} else if($logicOperator != NULL) {

			$temporaryConditionString .= ' ' . $logicOperator . ' ';

		}

		$temporaryConditionString .= $fieldName . ' ' . $operator . ' ';
		$temporaryConditionString .= ':cond' . count($this->conditionElements) .' ';

		$this->conditionString .= $temporaryConditionString;
		$this->conditionElements['cond' . count($this->conditionElements)] = $value;


	}

	//FIXME: set to private.
	public function getRequestString() {

		$requestString = 'SELECT ';

		if($this->fieldsName != NULL) {

			$requestString .= implode(',', $this->fieldsName);


		} else {

			$requestString .= '*';

		}

		$requestString .= ' FROM ' . $this->tableName . ' ';

		if($this->conditionString != '') {

			$requestString .= $this->conditionString;

		}

		if($this->limit != NULL) {

			$requestString .= ' LIMIT ' . $this->limit;

		}

		return $requestString;

	}

	public function performRequest() {

		$request = $this->connector->connector->prepare($this->getRequestString());
		$request->execute($this->conditionElements);

		$this->resultElement = $request;

		$this->readOnly = TRUE;

	}

	public function toArray() {

		if(!$this->readOnly) {

			$this->performRequest();

		}

		return $this->resultElement->fetchAll(PDO::FETCH_ASSOC);

	}

}
