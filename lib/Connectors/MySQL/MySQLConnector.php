<?php
class MySQLConnector extends SQLiteConnector {

	public function __construct($username, $password, $host, $database) {

		$this->connector = new PDO('mysql:host=' . $host .';dbname=' . $database, $username, $password);
		$this->connector->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
		$this->connector->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$this->connector = new PDO('mysql:host=' . $host .';dbname=' . $database, $username, $password);

		$tablesQuery = $this->connector->query('SHOW TABLES;');

		$this->tables = $tablesQuery->fetchAll(PDO::FETCH_COLUMN);
		$this->tableRows = [];

	}

	public function createRequest($requestType) {

		if($requestType == RequestType::REQUEST_SELECT) {

			return new MySQLSelectRequest($this);

		} else if($requestType == RequestType::REQUEST_INSERT) {

			return new MySQLInsertRequest($this);

		} else if($requestType == RequestType::REQUEST_DELETE) {

			return new MySQLDeleteRequest($this);

		} else if($requestType == RequestType::REQUEST_UPDATE) {

			return new MySQLUpdateRequest($this);

		} else {

			throw new DatabaseQueryError(_('Invalid request type'));

		}

	}

	public function isValidFieldName(string $tableName, string $fieldName) {

 		if(!$this->isValidTableName($tableName)) {

 			throw new DatabaseQueryError(_('Wrong table name in your query'));

 		}

 		if(!array_key_exists($tableName, $this->tableRows)) {

			$fieldsQuery = $this->connector->query('DESCRIBE ' . $tableName);
 			$this->tableRows[$tableName] = $fieldsQuery->fetchAll(PDO::FETCH_COLUMN);

 		}

 		return in_array($fieldName, $this->tableRows[$tableName]);
 	}

}

class MySQLDeleteRequest extends SQLiteDeleteRequest {}
class MySQLInsertRequest extends SQLiteInsertRequest {}
class MySQLSelectRequest extends SQLiteSelectRequest {}
class MySQLUpdateRequest extends SQLiteUpdateRequest {}

