<?php
include 'SQLite/autoload.php';
include 'MySQL/autoload.php';
include 'Requests/autoload.php';

spl_autoload_register(function ($class_name) {
	if(file_exists(dirname(__FILE__) . '/' . $class_name . '.php')) {
		include $class_name . '.php';
	}
});


