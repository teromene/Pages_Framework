<?php
class DatabaseReflectedObject {

	private $key;

	private $properties = array();

	private $triggerMethods = array();

	private $tableName = null;

	private $joinedVariables = array();

	private $joinedSingleVariables = array();

	/** Executed when one of the class extending DatabaseReflectedObject calls a function.
	It checks wether the method is a synchronised one, and if it is calls the ```sync``` function to resynchronise the data
	**/
	public function __call(string $name , array $arguments) {

		if(!method_exists(get_class($this), $name)) {

			throw new Exception(_('Method ' . $name . ' does not exists in class ' . get_class($this)));

		}

		$result = call_user_func_array(array($this, $name), $arguments);

		if(in_array($name, $this->triggerMethods)) {

			$this->sync();

		} else {

			throw new Exception("Function does not exists !");

		}

		return $result;

	}

	/** Fills the objects field with data pulled from the database, based on the object's primary key**/
	public function setupObject() {

		$className = get_class($this);
		
		while(get_parent_class($className) != 'DatabaseReflectedObject') {
			
			if(get_parent_class($className) == false) {
				
				throw new Exception("Invalid class " . $className . " loaded for DatabaseReflectedObject");
				
			}
			
			$className = get_parent_class($className);
			
		}
		
		$classInfo = new ReflectionClass($className);
		
		
		// Get the table name where the object's data are from
		if(preg_match('/@tableName ([^ \n]*)/', $classInfo->getDocComment(), $matches) > 0) {

			if(count($matches) == 2) {

				$this->tableName = $matches[1];

			} else {

				throw new Exception("Unable to parse table info for class " . get_class($this));

			}

		} else {

			throw new Exception("Unable to parse table info for class " . get_class($this));

		}

		//For every property, create the objects used to hold them
		foreach($classInfo->getProperties() as $property) {

			if(preg_match('/@database ([^ \n]*)[ ]*([^ \n]*)[ ]*([^ \n]*)[ ]*([^ \n]*)[ ]*([^ \n]*)[ ]*([^ \n]*)/', $property->getDocComment(), $matches) > 0) {

				if(count($matches) == 7) {

					$propertyName = $property->getName();

					if($matches[1] == "key") {

						$this->key = new Property($propertyName, $matches[2], $this->$propertyName);

					} else if($matches[1] == "value") {

						$this->properties[] = new Property($propertyName, $matches[2], $this->$propertyName);

					} else if($matches[1] == "joined") {

						$this->joinedVariables[] = new JoinedObject($propertyName, $matches[2], $matches[3], $matches[4], $matches[5], $matches[6]);

					} else if($matches[1] == "singleJoined") {

						$this->joinedSingleVariables[] = new SingleJoinedObject($propertyName, $matches[2], $matches[3], $matches[4]);

					} else {

						throw new Exception("Unable to parse synchronised variable field for property " . $propertyName);

					}

				}

			}

		}

		// Check if class methods are trigger methods, i.e methods that requires a resync when called.
		foreach($classInfo->getMethods() as $method) {

			if($method->class == get_class($this) && $method->isProtected()) {

				if(preg_match('/@synced/', $method->getDocComment(), $matches) > 0) {

					$this->triggerMethods[] = $method->getName();

				}


			}
		}

		//Now, we get the row corresponding to the object in the database, in order to fill the object"s properties.
		$valuesRequest = $GLOBALS['database']->createRequest(RequestType::REQUEST_SELECT);
		$valuesRequest->setTargetTable($this->tableName);

		$valuesRequest->setLimit(1);
		$valuesRequest->addCondition($this->key->propertyColumn, '=', $this->key->propertyValue);
		$valuesRequest->performRequest();

		$propertiesValues = $valuesRequest->toArray();

		if(count($propertiesValues) != 1) {

			throw new DatabaseQueryError(_('No data found for fields ' . $this->key->propertyColumn . ' in table ' . $this->tableName . '( key='. $this->key->propertyValue . ')'));

		}

		// Select the first row, as there is only one
		$propertiesValues = $propertiesValues[0];

		// Fill the object properties, as well as the DatabaseReflectedObject metadatas
		foreach($this->properties as $property) {

			if(array_key_exists($property->propertyColumn, $propertiesValues)) {

				$propertyName = $property->propertyName;
				$this->$propertyName = $propertiesValues[$property->propertyColumn];
				$property->propertyValue = $this->$propertyName;

			}

		}

		//Now, we process variables that are of type ```joinedVariable```
		foreach($this->joinedVariables as $joinedVariable) {

			$joinedTableRequest = $GLOBALS['database']->createRequest(RequestType::REQUEST_SELECT);

			$joinedTableRequest->setTargetTable($joinedVariable->joinTable);
			$joinedTableRequest->setRequestedFields(array($joinedVariable->targetClassSelectionKey));
			$joinedTableRequest->addCondition($joinedVariable->classSelectionKey, '=', $this->key->propertyValue);
			$joinedTableRequest->performRequest();

			$joinedTableElements = $joinedTableRequest->toArray();

			foreach($joinedTableElements as $joinedTableElement) {

				$joinedId = $joinedTableElement[$joinedVariable->targetClassSelectionKey];

				$targetClassName = $joinedVariable->targetClass;
				$targetElement = new $targetClassName($joinedId);
				
				if($joinedVariable->namedArrayFieldName != '') {
					
					$namedArrayFieldName = $joinedVariable->namedArrayFieldName;
					$keyValue = $targetElement->$namedArrayFieldName;
					$joinedVariable->values[$keyValue] = new $targetClassName($joinedId);
					
				} else {
					
					$joinedVariable->values[] = new $targetClassName($joinedId);

					
				}
				

			}

			$joinedVariableName = $joinedVariable->propertyName;
			$this->$joinedVariableName = $joinedVariable->values;

		}

		//Now we do the same for variables of type ````singleJoinedVariables```.
		foreach($this->joinedSingleVariables as $joinedSingleVariable) {

			$joinedClassName = $joinedSingleVariable->targetClass;
			$joinedPropertyName = $joinedSingleVariable->propertyName;

			$joinedPropertyId = $GLOBALS['database']->createRequest(RequestType::REQUEST_SELECT);
			$joinedPropertyId->setTargetTable($this->tableName);
			$joinedPropertyId->setLimit(1);
			$joinedPropertyId->addCondition($this->key->propertyColumn, '=', $this->key->propertyValue);
			$joinedPropertyId->setRequestedFields([$joinedSingleVariable->selectionColumn]);
			$joinedPropertyId->performRequest();

			$resultId = $joinedPropertyId->toArray();

			if(count($resultId) == 1) {
				try {

					$joinedSingleVariable->value = new $joinedClassName($resultId[0][$joinedSingleVariable->selectionColumn]);
					$this->$joinedPropertyName = $joinedSingleVariable->value;

				} catch(DatabaseQueryError $e) {

					$joinedSingleVariable->value = null;
					$this->$joinedPropertyName = null;

				}

			} else {

				$joinedSingleVariable->value = null;
				$this->$joinedPropertyName = null;

			}

		}

	}

	/** This function is the core of the DatabaseReflectedObject.
	When this method is called, it finds whether the object's properties values and the values stored in the DatabaseReflectedObject metadatas are different.
	If this is the case, we send the modification to the database.
	**/
	public function sync() {

		if($this->key == null) {

			throw new DatabaseQueryError(_('Unable to synchronise object without a key.'));

		}

		//In order to update all the values at once we create a single request here
		$request = $GLOBALS['database']->createRequest(RequestType::REQUEST_UPDATE);
		$request->setTargetTable($this->tableName);
		$request->addTargetCondition($this->key->propertyColumn, '=', $this->key->propertyValue);

		$needsUpdate = false;
		foreach($this->properties as $property) {

			$propertyName = $property->propertyName;
			if($property->propertyValue != $this->$propertyName) {

				$needsUpdate = true;
				$request->addUpdateField($property->propertyColumn, $this->$propertyName);

			}

		}

		foreach($this->joinedSingleVariables as $joinedSingleVariable) {

			$propertyName = $joinedSingleVariable->propertyName;

			if($joinedSingleVariable->value != $this->$propertyName) {

				$keyName = $joinedSingleVariable->targetKeyName;
				$needsUpdate = true;
				$request->addUpdateField($joinedSingleVariable->selectionColumn, $this->$propertyName->$keyName);

			}

		}


		if($needsUpdate) $request->performRequest();

		//We now find the difference in the joinedVariables. We can't do that with the previous request, as the joinedVariables are arrays of values.
		foreach($this->joinedVariables as $joinedVariable) {

			$joinedVariableName = $joinedVariable->propertyName;
			$targetClassKey = $joinedVariable->targetClassSelectionKey;


			//Manages the values that are not in the copy array, but in the new one (e.g deleted elements)
			foreach($joinedVariable->values as $value) {

				$found = false;
				foreach($this->$joinedVariableName as $secondValue) {

					if($value == $secondValue) {

						$found = true;

					}

				}

				//If we didn't find the value in the stored copy (deleted value)
				if(!$found) {
				
					$deleteLinkRequest = $GLOBALS['database']->createRequest(RequestType::REQUEST_DELETE);
					$deleteLinkRequest->setTargetTable($joinedVariable->joinTable);
					$deleteLinkRequest->addCondition($joinedVariable->classSelectionKey, '=', $this->key->propertyValue);
					$deleteLinkRequest->addCondition($joinedVariable->targetClassSelectionKey, '=', $value->getKeyValue(), 'AND');

					$deleteLinkRequest->performRequest();

				}

			}

			//Manages the values that are in the variable array, but not in the copy array (e.g inserted elements)
			foreach($this->$joinedVariableName as $value) {
				
				$found = false;
				foreach($joinedVariable->values as $secondValue) {

					if($value == $secondValue) {

						$found = true;

					}

				}

				//If we didn't find the value in the copy object (inserted element)
				if(!$found) {

					$insertLinkRequest = $GLOBALS['database']->createRequest(RequestType::REQUEST_INSERT);
					$insertLinkRequest->setTargetTable($joinedVariable->joinTable);
					$insertLinkRequest->setIgnoreExisting(true);
					$insertLinkRequest->setInsertedFields([$joinedVariable->classSelectionKey, $joinedVariable->targetClassSelectionKey]);
					$insertLinkRequest->setValues([$this->key->propertyValue, $value->getKeyValue()]);

					$insertLinkRequest->insert();
					
				}

			}

		}

	}


	public function getKeyValue() {

		return $this->key->propertyValue;

	}

	public static function createInstance(string $targetClass, array $values) {

		$classInfo = new ReflectionClass($targetClass);

		if(preg_match('/@tableName ([^ \n]*)/', $classInfo->getDocComment(), $matches) > 0) {

			if(count($matches) == 2) {

				$tableName = $matches[1];

			}

		} else {

			throw new DatabaseQueryError(_('Creating an instance of a non-reflected object !'));

		}

		try {

			$request = $GLOBALS['database']->createRequest(RequestType::REQUEST_INSERT);
			$request->setTargetTable($tableName);
			$request->setInsertedFields(array_keys($values));
			$request->setValues(array_values($values));
			$request->insert();

			return new $targetClass($request->getLastInsertId());

		} catch(DatabaseQueryError $e) {

			throw new DatabaseQueryError(_('Unable to create object : ' . $e . ', object :' . get_class($this)));

		}
	}

	protected static function listObjects(string $targetClass, array $filter) {

		$classInfo = new ReflectionClass($targetClass);

		foreach($classInfo->getProperties() as $property) {

			if(preg_match('/@database key ([^ \n]*)/', $property->getDocComment(), $matches) > 0) {

				$targetKey = $matches[1];

			}
		}

		if(preg_match('/@tableName ([^ \n]*)/', $classInfo->getDocComment(), $matches) > 0) {

			if(count($matches) == 2) {

				$tableName = $matches[1];

			}

		} else {

			throw new DatabaseQueryError(_('Can\'t list instances of non-reflected object ') . $targetClass);

		}

		$classListInfo = $GLOBALS['database']->createRequest(RequestType::REQUEST_SELECT);
		$classListInfo->setTargetTable($tableName);

		foreach($filter as $fieldName => $fieldValue) {
			$classListInfo->addCondition($fieldName, '=', $fieldValue);
		}

		$classListInfo->setRequestedFields([$targetKey]);
		$classListInfo->performRequest();

		$resultArray = [];
		foreach($classListInfo->toArray() as $classKey) {

			$classKey = $classKey[$targetKey];

			$resultArray[] = new $targetClass($classKey);

		}

		return $resultArray;
	}

}

class Property {

	public $propertyName;

	public $propertyColumn;

	public $propertyValue;

	public function __construct($propertyName, $propertyColumn, $propertyValue) {

		$this->propertyName = $propertyName;
		$this->propertyColumn = $propertyColumn;
		$this->propertyValue = $propertyValue;

	}

}

class JoinedObject {

	public $propertyName;

	public $targetClass;

	public $joinTable;

	public $classSelectionKey;

	public $targetClassSelectionKey;

	public $values;
	
	public $namedArrayFieldName;

	public function __construct($propertyName, $targetClass, $joinTable, $classSelectionKey, $targetClassSelectionKey, $namedArrayFieldName) {

		$this->propertyName = $propertyName;
		$this->targetClass = $targetClass;
		$this->joinTable = $joinTable;
		$this->classSelectionKey = $classSelectionKey;
		$this->targetClassSelectionKey = $targetClassSelectionKey;
		$this->namedArrayFieldName = $namedArrayFieldName;
		$this->values = array();

	}


}

class SingleJoinedObject {

	public $propertyName;

	public $targetClass;

	public $selectionColumn;

	public $targetKeyName;

	public $value;

	public function __construct($propertyName, $targetClass, $selectionColumn, $targetKeyName) {

		$this->propertyName = $propertyName;
		$this->targetClass = $targetClass;
		$this->selectionColumn = $selectionColumn;
		$this->targetKeyName = $targetKeyName;


	}


}
