<?php
/**
* This is where all the configuration management of Pages takes place.
*/
/**
* Class used to load the configuration and to pass it to Slim.
*/
class ConfigurationManager {

	/** @var integer the minimum number of urls that can be generated. */
	private $MIN_URL_CHARS = 2 ** 48;

	/** @var string The name of your Pages instance. */
	private $instanceName = null;

	/** @var DatabaseConnector the variable containing the connector to the database.*/
	private $database = null;

	/** @var string The database type. Can be SQLite or MySQL. Filled when the constructor is loaded.*/
	private $databaseType = null;

	/** @var string SQLite only : The file where the database is stored. Filled when the constructor is loaded.*/
	private $filename = null;

	/** @var string MySQL only : The username of the MySQL database. Filled when the constructor is loaded.*/
	private $username = null;

	/** @var string MySQL only : The password of the MySQL database. Filled when the constructor is loaded.*/
	private $password = null;

	/** @var string MySQL only : The host of the MySQL database. Filled when the constructor is loaded.*/
	private $host = null;

	/** @var string MySQL only : The database used by MySQL. Filled when the constructor is loaded.*/
	private $databaseName = null;

	/** @var boolean Boolean variable representing wether we allow creating custom urls or not. The default is false. Filled when the constructor is loaded.*/
	private $allowCustomUrls = null;

	/** @var string Prefix used to access the custom URLs. The default is null. Filled when the constructor is loaded.*/
	private $customUrlPrefix = null;

	/** @var array Chars used to create the URLs. The default is a-zA-Z0-9. Filled when the constructor is loaded.*/
	private $customUrlChars = null;

	/** @var integer Number of chars used to create the URLs. The default is 48. The number of possible urls created should always be higher than 2^48. Filled when the constructor is loaded.*/
	private $customCharsCount = null;

	/**
	* Creates a new ConfigurationManager instance from a specific configuration file.
	* If no file is specified, it tries to load it from 'conf.json'.
	*
	* @param string $fileName the name of the file to load. The default is 'conf.json'.
	* @param boolean $connect wether to connect right away to the database.
	* @return ConfigurationManager
	* @throws ConfigurationErrorException
	*/
	public function __construct($fileName = 'conf.json', $connect = TRUE) {

		$this->loadDefaultConfiguration();

		try {

			$configuration = json_decode(file_get_contents($fileName), TRUE);

		} catch(Exception $e) {

			throw new ConfigurationErrorException(_('Unable to load the specified file.'));

		}
		//If we can't find a valid configuration file at specified position, we throw an exception
		if($configuration == null) throw new ConfigurationErrorException(_('Couldn\'t load configuration from ' . $fileName));

		//We check that the mandatory configuration variables exists
		if(!array_key_exists('database', $configuration) || !array_key_exists('type', $configuration['database'])) throw new ConfigurationErrorException(_('Missing the database configuration key in the configuration file %s', $fileName));


		//Check that the database options are valid
		if(strtolower($configuration['database']['type']) == 'sqlite') {

			if(!array_key_exists('filename', $configuration['database']) || $configuration['database']['filename'] == null) {

				throw new ConfigurationErrorException(_('The database selected is \'sqlite\', but no filename was specified'));

			}

			$this->filename = $configuration['database']['filename'];
			$this->databaseType = 'sqlite';
			if($connect) $this->database = new SQLiteConnector($this->filename);



		} else if(strtolower($configuration['database']['type']) == 'mysql') {

			if(!array_key_exists('username', $configuration['database']) || !array_key_exists('password', $configuration['database']) || !array_key_exists('host', $configuration['database']) || !array_key_exists('database', $configuration['database']) || $configuration['database']['username'] == null || $configuration['database']['host'] == null  || $configuration['database']['password'] == null || $configuration['database']['host'] == null) {

				throw new ConfigurationErrorException(_('The database selected is \'mysql\', but some informations are missing'));

			}

			$this->username = $configuration['database']['username'];
			$this->password = $configuration['database']['password'];
			$this->host = $configuration['database']['host'];
			$this->databaseName = $configuration['database']['database'];

			$this->databaseType = 'mysql';
			if($connect) $this->database = new MySQLConnector($this->username, $this->password, $this->host, $this->databaseName);

		} else {

			throw new ConfigurationErrorException(_('The database type supplied is not valid'));

		}

		//Perform checks for the URL configuration
		if(array_key_exists('urls', $configuration)) {

			if(array_key_exists('allowCustomUrls', $configuration['urls'])) {


				$this->allowCustomUrls = $configuration['urls'] ? TRUE : FALSE;

			}

			if(array_key_exists('customUrlPrefix', $configuration['urls'])) {

				$this->customUrlPrefix = $configuration['urls']['customUrlPrefix'];

			}

			if(array_key_exists('customUrlChars', $configuration['urls'])) {

				$this->customUrlChars = array_unique(str_split($configuration['urls']['customUrlChars']));

			}

			if(array_key_exists('customCharsCount', $configuration['urls'])) {

				$this->customCharsCount = $configuration['urls']['customCharsCount'];

			}

			if(pow(count($this->customUrlChars), $this->customCharsCount) < $this->MIN_URL_CHARS) {

				throw new ConfigurationErrorException(_('The number of possible urls is not high enough, should be more than %d', $this->MIN_URL_CHARS));

			}

		}

	}


	/**
	* Loads the default base configuration by setting the class variables accordingly
	* @return void
	*/
	private function loadDefaultConfiguration() {

		//Setting default values

		$this->instanceName = _('My Pages instance');

		$this->allowCustomUrls = false;
		$this->customUrlChars = array_merge(range('a', 'z'), range('A', 'Z'), range('0', '9'));
		$this->customCharsCount = 48;

	}

	/**
	* Exports the default configuration into a Slim parsable array
	* @return Array a slim parsable configuration array
	*/
	public function getSlimConfiguration() {

		return [
			'settings' => [
				'displayErrorDetails' => true,
				'addContentLengthHeader' => false,

				'instanceName' => $this->instanceName,
				'database' => $this->database,

				'urls' => [
					'allowCustomUrls' => $this->allowCustomUrls,
					'customUrlPrefix' => $this->customUrlPrefix,

					'customUrlChars' => $this->customUrlChars,
					'customCharsCount' => $this->customCharsCount,

				],


				// Renderer settings
				'renderer' => [
				    'template_path' => '../views/',
				],

				// Monolog settings
				'logger' => [
				    'name' => 'Pages - ' . $this->instanceName,
				    'path' => '../logs/app.log',
				    'level' => \Monolog\Logger::DEBUG,
				],
			],
		];

	}

	/**
	* Register elements needed by Slim, like the view renderer and the logger.
	* @param \Slim\App $app the Slim app where we register our elements.
	*/
	private function registerSlimDependencies(\Slim\App $app) {

		$container = $app->getContainer();

		// view renderer
		$container['view'] = function ($container) {
			$view = new \Slim\Views\Twig('views');

			// Instantiate and add Slim specific extension
			$basePath = rtrim(str_ireplace('index.php', '', $container['request']->getUri()->getBasePath()), '/');
			$view->addExtension(new Slim\Views\TwigExtension($container['router'], $basePath));
			$view->addExtension(new TwigConstantCall());
			$view->getEnvironment()->addGlobal('path', str_replace("/index.php", "", $container['request']->getUri()->getBasePath()));
			return $view;
		};

		// monolog
		$container['logger'] = function ($c) {
		    $settings = $c->get('settings')['logger'];
		    $logger = new Monolog\Logger($settings['name']);
		    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
		    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
		    return $logger;
		};

	}

	/**
	* Generates the Slim app based on the configuration.
	*/
	public function generateApp() {

		$app = new \Slim\App($this->getSlimConfiguration());
		$this->registerSlimDependencies($app);

		return $app;

	}

	/**
	* Returns the name of the instance.
	* @return string the instance name.
	*/
	public function getInstanceName() {

		return $this->instanceName;

	}

	/**
	* Returns the connector to the database.
	* @return DatabaseConnector the connector.
	*/
	public function getDatabase() {

		return $this->database;

	}

	/**
	* Returns the type of the database.
	* @return string the database type.
	*/
	public function getDatabaseType() {

		return $this->databaseType;

	}

	/**
	* Sqlite only. Returns the filename to the Sqlite database.
	* @return string the path to the sqlite database.
	*/
	public function getDatabaseFilename() {

		return $this->filename;

	}

	/**
	* MySQL only. Returns the username of the MySQL database.
	* @return string the username of the MySQL database.
	*/
	public function getDatabaseUsername() {

		return $this->username;

	}

	/**
	* MySQL only. Returns the password of the MySQL database.
	* @return string the password of the MySQL database.
	*/
	public function getDatabasePassword() {

		return $this->password;

	}

	/**
	* MySQL only. Returns the host of the MySQL database.
	* @return string the host of the MySQL database.
	*/
	public function getDatabaseHost() {

		return $this->host;

	}

	/**
	* MySQL only. Returns the name of the MySQL database.
	* @return string the name of the MySQL database.
	*/
	public function getDatabaseName() {

		return $this->databaseName;

	}


	/**
	* Returns whether we allow custom urls or not.
	* @return boolean true if we allow custom urls, and false otherwise.
	*/
	public function getAllowCustomUrl() {

		return $this->allowCustomUrls;

	}

	/**
	* Returns the prefix used for custom urls.
	* @return string the custom url prefix. Note that this variable has a value even if allowCustomUrls is set to false.
	*/
	public function getCustomUrlPrefix() {

		return $this->customUrlPrefix;

	}

	/**
	* Return the chars that makes the urls.
	* @return Array the chars used to create the urls
	*/
	public function getCustomUrlChars() {

		return $this->customUrlChars;

	}

	/**
	* Return the number of characters used to create an url.
	* @return integer the number of characters used to create an url.
	*/
	public function getCustomCharsCount() {

		return $this->customCharsCount;

	}
}

